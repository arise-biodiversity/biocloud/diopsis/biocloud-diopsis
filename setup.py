from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

with open('requirements.txt') as f:
    required = [line for line in f.read().splitlines() if not line.startswith('-')]

setup(
    name='diopsis',
    version='0.1.1',
    author='Team biocloud',
    url='https://gitlab.com/arise-biodiversity/biocloud/diopsis/biocloud-diopsis',
    author_email='',
    description='biocloud-diopsis',
    packages=find_packages(exclude=["tests", "tests.*"]),
    entry_points={
        'group_1': 'run=diopsis.__main__:main'
    },
    install_requires=required,
    dependency_links=['https://gitlab.com/api/v4/groups/12875264/-/packages/pypi/simple'],
    long_description=long_description,
    long_description_content_type='text/markdown'
)
