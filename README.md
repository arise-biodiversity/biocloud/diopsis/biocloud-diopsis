# Biocloud Diopsis

The Diopsis application is part of the biocloud setup,
it is meant to be run combined with core.

You can run the application locally by running src/__main__.py.

## Setting up the local development environment

You can develop and debug in the IDE while running code through Databricks connect:

First make a virtual environment:
python -m venv venv

Activate it:
source venv/bin/activate

install requirements:
pip install -r requirements.txt
pip install -r requirements-test.txt

Set in the diopsis/config.py file:
SPARK_ENVIRONMENT: str = 'LOCAL'

Make sure you set the required env. variables in e.g. a .env file:

- `FAUNABIT_API_URL`
- `FAUNABIT_TOKEN`
- `S3_REGION_NAME`
- `S3_ACCESS_KEY_ID`
- `S3_SECRET_ACCESS_KEY`
- `S3_LANDING_ZONE_BUCKET`
- `DIOPSIS_PAYLOAD_URL`
- `DIOPSIS_PAYLOAD_USER`
- `DIOPSIS_PAYLOAD_PASS`
- `DIOPSIS_PAYLOAD_REQUEST_TIMEOUT_CONNECT`
- `DIOPSIS_PAYLOAD_REQUEST_TIMEOUT_RESPONSE`

When no value is provided for `FAUNABIT_API_URL`, the app will use [https://api.faunabit.eu/api/](https://api.faunabit.eu/api/) as default value.

When no value is provided for `DIOPSIS_PAYLOAD_URL`, the app will use [https://ai.naturalis.nl/v1](https://ai.naturalis.nl/v1) as default value.

When no value is provided for `DIOPSIS_PAYLOAD_REQUEST_TIMEOUT_CONNECT`, the app will use __30__ as default value.

When no value is provided for `DIOPSIS_PAYLOAD_REQUEST_TIMEOUT_RESPONSE`, the app will use __150__ as default value.

When any of the other variables has no value, the application will fail to run.

## Arguments

It is possible to add (command line) arguments when running biocloud diopsis. It is not required because there are already default values for this variables in the `config.py` file or even as a `.env` variable, but sometimes it could be useful when running the diopsis pipeline in databricks.

The following arguments are allowed:

- `--camera_numbers`
- `--max_payload_files_per_device`
- `--max_pool_connections`
- `--parallel_downloads`
- `--startdate_filter`
- `--enddate_filter`

They follow the same rules as the config / env variables `CAMERA_NUMBERS`, `MAX_PAYLOAD_FILES_PER_DEVICE`, `S3_MAX_POOL_CONNECTIONS`, `PARALLEL_DOWNLOADS`, `STARTDATE_FILTER` and `ENDDATE_FILTER` respectively.
To test locally on vscode, the following settings can be added to the `launch.json` file with the needed arguments, for example:

```json
{
  "version": "0.2.0",
  "configurations": [
    {
      "name": "Diopsis Args",
      "type": "debugpy",
      "request": "launch",
      "module": "diopsis.__main__",
      "args": [
          "--camera_numbers",
          "232,278",
          "--max_payload_files_per_device",
          "-1",
          "--max_pool_connections",
          "30",
          "--parallel_downloads",
          "20"
      ],
      "justMyCode": true
    }
  ]
}
```
