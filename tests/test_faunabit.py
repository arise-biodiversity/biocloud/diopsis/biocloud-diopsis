import pytest
from diopsis.config import Config
from diopsis.faunabit.faunabit_api import FaunabitAPI


@pytest.fixture(scope="class")
def faunabit() -> FaunabitAPI:
    """Fixture for Faunabit test class."""
    """FIX_ME: This token should come from the spark environment"""
    return FaunabitAPI(Config.FAUNABIT_API_URL, "fix_me_token")


@pytest.mark.usefixtures("faunabit")
@pytest.mark.skip(reason="Not a unit test. Needs external systems and spark environment to run.")
class TestFaunabit:
    """Test the Faunabit class."""

    def test_get_faunabit_data_for_locations(self, faunabit: FaunabitAPI) -> None:
        """Test getting locations data from Faunabit."""
        response = faunabit._get_faunabit_resource("locations")

        assert response is not None
        assert len(response['locations']) > 0

    def test_get_faunabit_data_for_researches(self, faunabit: FaunabitAPI) -> None:
        """Test getting researches data from Faunabit."""
        response = faunabit._get_faunabit_resource("researches")

        assert response is not None
        assert len(response) > 0

    def test_get_faunabit_data_for_devices(self, faunabit: FaunabitAPI) -> None:
        """Test getting devices data from Faunabit."""
        response = faunabit._get_faunabit_resource("devices")

        assert response is not None
        assert len(response) > 0
