# Databricks notebook source
# MAGIC %md
# MAGIC This notebook installs required dependencies as listed in the requirements.txt files, restarts python and then executes any found pytest unit tests. This is the recommended approach as documented here: https://docs.databricks.com/en/notebooks/testing.html#run-unit-tests. A practical example with implementation can be found here: https://github.com/databricks/notebook-best-practices/.

# COMMAND ----------

!cp ../requirements.txt ~/.
!cp ../requirements-test.txt ~/.
%pip install -r ~/requirements.txt
%pip install -r ~/requirements-test.txt

# COMMAND ----------

# pytest.main runs our tests directly in the notebook environment, providing
# fidelity for Spark and other configuration variables.
#
# A limitation of this approach is that changes to the test will be
# cache by Python's import caching mechanism.
#
# To iterate on tests during development, we restart the Python process 
# and thus clear the import cache to pick up changes.
dbutils.library.restartPython()

# COMMAND ----------

# Automatically reloads any changed python libraries that are imported.
# This is to prevent having to detach/reattach to the cluster ever time you changed the code.
%load_ext autoreload
%autoreload 2

import pytest
import os
import sys

# Run all tests in the repository root.
# Get the path to this notebook, for example "/Workspace/Repos/{username}/{repo-name}".
notebook_path = dbutils.notebook.entry_point.getDbutils().notebook().getContext().notebookPath().get()

# Get the repo's root directory name.
repo_root = os.path.dirname(os.path.dirname(notebook_path))

# Prepare to run pytest from the repo.
os.chdir(f'/Workspace/{repo_root}')
print(os.getcwd())

# Skip writing pyc files on a readonly filesystem.
sys.dont_write_bytecode = True

retcode = pytest.main([".", "-v", "-p", "no:cacheprovider"])

# Fail the cell execution if we have any test failures.
assert retcode == 0, 'The pytest invocation failed. See the log above for details.'
