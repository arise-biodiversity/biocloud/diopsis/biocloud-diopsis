from biocloud_library.storage import S3Service
from diopsis.config import Config


def s3_service(spark) -> S3Service:
    max_pool_connections = int(
        Config._get_arg_val("--max_pool_connections", Config.S3_MAX_POOL_CONNECTIONS)
    )

    return S3Service(
        Config.get_secret(spark, "S3_ACCESS_KEY_ID"),
        Config.get_secret(spark, "S3_SECRET_ACCESS_KEY"),
        Config.S3_ENDPOINT,
        Config.S3_REGION_NAME,
        max_pool_connections
    )
