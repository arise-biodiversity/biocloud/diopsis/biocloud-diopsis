import logging
import sys
import os
from typing import get_type_hints, Union, Optional
from dotenv import load_dotenv

load_dotenv()


class AppConfigError(Exception):
    pass


def _get_dbutils(spark):
    from pyspark.dbutils import DBUtils
    return DBUtils(spark)


def _parse_bool(val: Union[str, bool]) -> bool:  # pylint: disable=E1136
    return val if isinstance(val, bool) else val.lower() in ['true', 'yes', '1']


def _parse_optional_str(val: Union[str, None]) -> Optional[str]:
    return None if val.lower() in ["none", "null", ""] else str(val)


# AppConfig class with required fields, default values, type checking, and typecasting for int and bool values
# The environment variables are defined in the cluster under Computer -> {clustername} -> Edit -> Advanced Options -> Spark.
# Secrets are managed by Databricks and can be added through the Databricks CLI.
class AppConfig:
    # DO NOT store "sensible" secrets variables here
    # Use the get_secrets() methods to access them
    LOGGING_LEVEL: int = 20  # 10 DEBUG, 20 INFO, 30 WARNING, 40 ERROR

    # The environment you are developing in (TEST or PROD)
    ENVIRONMENT: str = 'TEST'

    # The name of the Databricks profile specified in your .databrickscfg file
    DATABRICKS_PROFILE: str = 'DEVELOPMENT'

    # Local or Databricks, depending on where you are developing
    SPARK_ENVIRONMENT: str = 'LOCAL'

    # Camera numbers to process (separated by comma)
    CAMERA_NUMBERS: str = '413,278'

    # Apply date filters, date format 'yyyy-mm-dd' or 'None' if no filter has to be applied
    STARTDATE_FILTER: Optional[str] = None
    ENDDATE_FILTER: Optional[str] = None
    MAX_PAYLOAD_FILES_PER_DEVICE: int = 50  # Limit number of files per device to process. No limit: -1, no payloads 0.
    S3_MAX_POOL_CONNECTIONS: int = 30

    DEBUGGER: bool = False
    FAUNABIT_API_URL: str = 'https://api.faunabit.eu/api/'
    S3_REGION_NAME: str = 'eu-west-1'
    S3_LANDING_ZONE_BUCKET: str
    S3_ENDPOINT: str = 'http://s3.eu-west-1.amazonaws.com'
    PARALLEL_DOWNLOADS: int = 20
    DIOPSIS_PAYLOAD_URL: str = 'https://ai.naturalis.nl/v1'
    DIOPSIS_PAYLOAD_REQUEST_TIMEOUT_CONNECT: int = 60
    DIOPSIS_PAYLOAD_REQUEST_TIMEOUT_RESPONSE: int = 60

    # Map environment variables to class fields according to these rules:
    #   - Field won't be parsed unless it has a type annotation
    #   - Field will be skipped if not in all caps
    #   - Class field and environment variable name are the same
    def __init__(self, env):
        for field in self.__annotations__:
            if not field.isupper():
                continue

            # Raise AppConfigError if required field not supplied
            default_value = getattr(self, field, None)
            if default_value is None and env.get(field) is None:
                raise AppConfigError(f'The {field} field is required')

            # Cast env var value to expected type and raise AppConfigError on failure
            try:
                var_type = get_type_hints(AppConfig)[field]
                if var_type == bool:
                    value = _parse_bool(env.get(field, default_value))
                elif var_type == Optional[str]:
                    value = _parse_optional_str(env.get(field, default_value))
                else:
                    value = var_type(env.get(field, default_value))

                self.__setattr__(field, value)
            except ValueError:
                raise AppConfigError(f'Unable to cast value of "{env[field]}" to type "{var_type}" for "{field}" field')

        if self.ENVIRONMENT not in ['TEST', 'PROD']:
            raise AppConfigError('The ENVIRONMENT field must be either "TEST" or "PROD"')
        if self.SPARK_ENVIRONMENT not in ['LOCAL', 'DATABRICKS']:
            raise AppConfigError('The SPARK_ENVIRONMENT field must be either "LOCAL" or "DATABRICKS"')

    def __repr__(self):
        return str(self.__dict__)

    # Get a "sensible" secret from the Databricks secret store
    # Examples:
    #     S3_ACCESS_KEY_ID
    #     S3_SECRET_ACCESS_KEY
    def get_secret(self, spark, key):
        if self.SPARK_ENVIRONMENT == 'LOCAL':
            return os.environ.get(key)
        elif self.SPARK_ENVIRONMENT == 'DATABRICKS':
            dbutils = _get_dbutils(spark)
            return dbutils.secrets.get(scope="diopsis-" + self.ENVIRONMENT, key=key)
        raise AppConfigError('Secret could not be retrieved. The SPARK_ENVIRONMENT field must be either "LOCAL" or "DATABRICKS"')

    def _get_arg_val(self, cmd_arg: str, config_val: str) -> str:
        """
        Get value from command-line arguments, otherwise return the provided default or env variable.
        """
        return (
            str(sys.argv[sys.argv.index(cmd_arg) + 1])
            if cmd_arg in sys.argv and sys.argv.index(cmd_arg) + 1 < len(sys.argv)
            else config_val
        )


# Expose Config object for app to import
Config = AppConfig(os.environ)
logging.basicConfig(level=Config.LOGGING_LEVEL)
