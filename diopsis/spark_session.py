from pyspark.sql import SparkSession

from diopsis.config import Config


def spark_session() -> SparkSession:
    if Config.SPARK_ENVIRONMENT == "DATABRICKS":
        # If we're running inside Databricks, we can just get the SparkSession
        return SparkSession.builder.getOrCreate()
    elif Config.SPARK_ENVIRONMENT == "LOCAL":
        # If we're running locally but via Databricks connect, we can use the DatabricksSession builder to create a SparkSession
        from databricks.connect import DatabricksSession

        return DatabricksSession.builder.profile(Config.DATABRICKS_PROFILE).getOrCreate()
    else:
        raise Exception(f"Invalid SPARK_ENVIRONMENT value: {Config.SPARK_ENVIRONMENT}")
