from diopsis.diopsis_app import DiopsisApp


def main():
    diopsis = DiopsisApp()
    diopsis.run()


if __name__ == "__main__":
    main()
