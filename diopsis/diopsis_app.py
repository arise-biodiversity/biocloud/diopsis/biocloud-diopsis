import concurrent.futures
from datetime import datetime
import json
import logging

from diopsis.config import Config
from diopsis.faunabit.faunabit_api import FaunabitAPI
from diopsis.faunabit.naturalis_server import NaturalisServer, get_image_metadata
from diopsis.s3_service import s3_service
from diopsis.spark_session import spark_session

logger = logging.getLogger(__name__)


class DiopsisApp:
    def __init__(self):
        self.spark = spark_session()
        self.s3 = s3_service(self.spark)
        self.camera_numbers = Config._get_arg_val("--camera_numbers", Config.CAMERA_NUMBERS).replace(" ", "").split(",")
        self.max_payload_files_per_device = int(Config._get_arg_val("--max_payload_files_per_device",
                                                Config.MAX_PAYLOAD_FILES_PER_DEVICE))
        self.startdate_filter = self._get_time_filter("--startdate_filter", Config.STARTDATE_FILTER)
        self.enddate_filter = self._get_time_filter("--enddate_filter", Config.ENDDATE_FILTER)

    def _get_time_filter(self, cmd_arg: str, config_val: str) -> datetime:
        """Transform filter value from string type to datetime type"""
        date_str = Config._get_arg_val(cmd_arg, config_val)
        filter_date = datetime.min
        try:
            if date_str:
                filter_date = datetime.strptime(date_str, "%Y-%m-%d")
        except ValueError:
            logger.error(f"Invalid date: {date_str} does not match format 'yyyy-mm-dd' ")
            if config_val:
                filter_date = datetime.strptime(config_val, "%Y-%m-%d")
        return filter_date

    def run(self):
        logger.info("Get all useful Faunabit metadata from the FaunabitAPI and importing it into the landing zone...")
        faunabit_api = FaunabitAPI(Config.FAUNABIT_API_URL, Config.get_secret(self.spark, "FAUNABIT_TOKEN"))
        self._process_faunabit_metadata(faunabit_api)

        if len(self.camera_numbers) > 0:
            logger.info(f"Camera numbers to be processed: {self. camera_numbers}")
            naturalis_server = NaturalisServer(Config.DIOPSIS_PAYLOAD_URL,
                                               Config.get_secret(self.spark, 'DIOPSIS_PAYLOAD_USER'),
                                               Config.get_secret(self.spark, 'DIOPSIS_PAYLOAD_PASS'))
            if self.max_payload_files_per_device == 0:
                logger.info("No payload will be processed. Change MAX_PAYLOAD_FILES_PER_DEVICE to process payload.")
            else:
                self._process_faunabit_images(naturalis_server, faunabit_api, self.camera_numbers)

    def _process_faunabit_metadata(self, faunabit_api: FaunabitAPI):
        """Get all useful Faunabit metadata from the FaunabitAPI and write it to the landing zone."""
        deployment_data = faunabit_api.get_faunabit_metadata()

        logger.info("Writing metadata to S3 landing zone.")
        for key in deployment_data.keys():
            json_file = f"diopsis/faunabit/{key}.json"

            logger.debug(f"Writing {json_file} to S3...")
            self.s3.put_object(Config.S3_LANDING_ZONE_BUCKET, json_file, json.dumps(deployment_data[key]))

        logger.info("Successfully fetched and stored Faunabit metadata in S3.")

    def _process_image_metadata(self, camera_number: str, device_id: str, image_ids: list[str]) -> None:
        """Create jsonl image metadata and store in landing zone bucket"""
        merged_json = ""
        for image_id in image_ids:
            merged_json += f"{json.dumps(get_image_metadata(camera_number, image_id, device_id))}\n"

        timestamp = int(datetime.now().timestamp())
        metadata_key = f"diopsis/sensors/{camera_number}/processed/metadata/merged-{timestamp}.jsonl"
        if not self.s3.exists_object(Config.S3_LANDING_ZONE_BUCKET, metadata_key):
            self.s3.put_object(Config.S3_LANDING_ZONE_BUCKET, metadata_key, merged_json)

    def _filter_date(self, image_ids: list[str]) -> list[str]:
        """Select only images of a certain year"""
        if self.enddate_filter == datetime.min:
            self.enddate_filter = datetime.now()
        invalid_ids = []
        selected_ids = []
        for image_id in image_ids:
            try:
                image_date = datetime.strptime(image_id[:8], "%Y%m%d")
                if self.startdate_filter <= image_date <= self.enddate_filter:
                    selected_ids.append(image_id)
            except ValueError as e:
                logger.error(f"Payload file {image_id} is not a valid datetime: {e}")
                invalid_ids.append(image_id)
        if len(invalid_ids) > 0:
            logger.info(f"WARNING: {len(invalid_ids)} invalid file names found: {invalid_ids}")
        logger.info(f"{len(selected_ids)} images found between {self.startdate_filter.strftime('%Y-%m-%d')} and {self.enddate_filter.strftime('%Y-%m-%d')}")
        return selected_ids

    def _search_device_id(self, camera_number: str, device_ids: list[dict]) -> str:
        """Search for the corresponding device_id that corresponds to the camera number (name) for metadata device_id field"""
        device_id = ""
        for local_id in device_ids:
            if camera_number == local_id["number"]:
                device_id = local_id["id"]
                break
        return device_id

    def _process_faunabit_images(self, naturalis_server: NaturalisServer, faunabit_api: FaunabitAPI, camera_numbers: list):
        """Get all image payloads for the chosen <camera_numbers> from https://ai.naturalis.nl server, generate image metadata and store it in S3."""
        device_ids = faunabit_api.get_device_ids()

        for camera_number in camera_numbers:
            device_id = self._search_device_id(camera_number, device_ids)
            if device_id == "":
                logger.error(f"Device id not found for camera number {camera_number}.")
                continue

            # Get image ids that belong to camera number
            image_ids = naturalis_server.get_image_ids(camera_number)

            # Apply year filter
            if self.startdate_filter > datetime.min or self.enddate_filter > datetime.min:
                image_ids = self._filter_date(image_ids)

            if self.max_payload_files_per_device > 0:
                logger.info(f"Only {self.max_payload_files_per_device} files will be processed. Change MAX_PAYLOAD_FILES_PER_DEVICE value to -1 to process all payload.")
                image_ids = image_ids[:self.max_payload_files_per_device]

            if len(image_ids) > 0:
                self._process_image_metadata(camera_number, device_id, image_ids)

                # Process image payload
                parallel_downloads = int(Config._get_arg_val("--parallel_downloads", Config.PARALLEL_DOWNLOADS))
                with concurrent.futures.ThreadPoolExecutor(max_workers=parallel_downloads) as executor:
                    for image_id in image_ids:
                        executor.submit(naturalis_server.process_image, self.s3, camera_number, image_id)

                logger.info(f"Successfully wrote {len(image_ids)} image payload and metadata from sensor {camera_number} to S3.")
        logger.info(f"Successfully fetched and stored image data in S3: {camera_numbers}")
