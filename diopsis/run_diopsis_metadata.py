# flake8: noqa
# Databricks notebook source
# Install the requirements and restart python. You don't have to execute this every time you want to run the code;
# it's only needed the first time, or after changing the requirements.txt file.
!cp ../requirements.txt ~/.
%pip install -r ~/requirements.txt

dbutils.library.restartPython()

# COMMAND ----------

# Automatically reloads any changed python libraries that are imported.
# This is to prevent having to detach/reattach to the cluster every time you changed the code.
%load_ext autoreload
%autoreload 2


from diopsis.__main__ import main

main()
