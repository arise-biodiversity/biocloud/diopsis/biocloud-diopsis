import logging
import requests

logger = logging.getLogger(__name__)


class FaunabitAPI:
    """Class for querying the Faunabit API to get the metadata."""

    API_RESOURCES = [
        'locations',
        'researches',
        'devices',
    ]

    def __init__(self, url: str, token: str):
        self.url = url
        self.token = token

    def get_faunabit_metadata(self) -> dict:
        deployment_data = {}
        deployment_data["deployments"] = self._get_faunabit_deployments()
        deployment_data["projects"] = self._get_faunabit_projects()
        deployment_data["locations"] = self._get_faunabit_locations()
        deployment_data["devices"] = self._get_faunabit_devices()

        logger.info("Successfully fetched all deployment metadata from Faunabit API.")
        return deployment_data

    def get_device_ids(self):
        devices = []
        json_obj = self._get_faunabit_resource("devices")
        for obj in json_obj:
            devices.append({"number": obj["name"][8:], "id": obj["id"]})
        return devices

    def _get_faunabit_deployments(self) -> list:
        logger.info("Fetching deployments from Faunabit API...")
        json_obj = self._get_faunabit_resource("researches")
        logger.info("Successfully fetched deployments from Faunabit API.")

        deployments = []
        for obj in json_obj:
            for deployment in obj["location_researches"]:
                deployments.append(deployment)
        return deployments

    def _get_faunabit_projects(self) -> list:
        logger.info("Fetching projects from Faunabit API...")
        json_obj = self._get_faunabit_resource("researches")
        logger.info("Successfully fetched projects from Faunabit API.")

        projects = []
        for obj in json_obj:
            deployments = []
            for deployment in obj["location_researches"]:
                deployments.append(deployment["id"])
            projects.append({"id": obj["id"], "name": obj["name"], "description": obj["description"],
                             "deployments": deployments, "organization_id": obj["organization_id"],
                             "start_date": obj["start_date"], "end_date": obj["end_date"]})
        return projects

    def _get_faunabit_locations(self) -> dict:
        logger.info("Fetching locations from Faunabit API...")
        json_obj = self._get_faunabit_resource("locations")
        logger.info("Successfully fetched locations from Faunabit API.")

        return json_obj["locations"]

    def _get_faunabit_devices(self) -> dict:
        logger.info("Fetching devices from Faunabit API...")
        json_obj = self._get_faunabit_resource("devices")
        logger.info("Successfully fetched devices from Faunabit API.")

        return json_obj

    def _get_faunabit_resource(self, resource: str):
        """Fetch metadata JSON files from Faunabit."""
        if resource not in self.API_RESOURCES:
            raise ValueError(f"Not a valid Faunabit resource: {resource}.")

        response = requests.get(
            f"{self.url}{resource}",
            headers={
                "Authorization": f"Bearer {self.token}"
            })

        try:
            response.raise_for_status()
        except requests.exceptions.HTTPError as e:
            logger.error(f"{response.status_code} {response.reason}")
            raise e

        return response.json()
